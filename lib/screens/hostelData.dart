import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:myhostel/screens/studentDetail.dart';

class HostelData extends StatefulWidget {
  const HostelData({Key? key}) : super(key: key);

  @override
  State<HostelData> createState() => _HostelDataState();
}

class _HostelDataState extends State<HostelData> {
  List<dynamic> users = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          "Students",
          style: TextStyle(color: Colors.black),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: fetchUsers,
        child: const Text('Fetch'),
      ),
      body: SafeArea(
        child: ListView.builder(
            itemCount: users.length,
            itemBuilder: (context, index) {
              final user = users[index];
              final thumbnail = user['picture']['thumbnail'];
              final fName = user['name']['first'];
              final lName = user['name']['last'];
              final email = user['email'];
              final profilePhoto = user['picture']['large'];
              final dob = user['dob']['age'];
              final username = user['login']['username'];
              final registeredOn = user['registered']['date'];
              final phoneNumber = user['phone'];
              final country = user['location']['country'];
              final gender = user['gender'];

              return Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: ListTile(
                  title: Row(
                    children: [
                      Text(fName),
                    ],
                  ),
                  leading: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(thumbnail),
                  ),
                  trailing: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => StudentDetail(
                                fName: fName,
                                lName: lName,
                                email: email,
                                profilePhoto: profilePhoto,
                                dob: dob.toString(),
                                userName: username,
                                phoneNumber: phoneNumber,
                                country: country,
                                registeredOn: registeredOn,
                                gender: gender,
                              )));
                    },
                    child: Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(
                            color: Colors.grey,
                            width: 1.0,
                          )),
                      child: const Icon(Icons.arrow_forward_ios_outlined),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }

  void fetchUsers() async {
    const url = 'https://randomuser.me/api/?results=20';
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    final body = response.body;
    final json = jsonDecode(body);

    setState(() {
      users = json['results'];
    });
    print('fetch user completed');
  }
}
