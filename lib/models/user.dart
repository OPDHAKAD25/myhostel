
class User {
  User({required this.results, required this.info});
  
  factory User.fromMap(Map<String, dynamic> map) => User(
      results: List<Results>.from(map['results'].map((e) => Results.fromMap(e))),
      info: Info.fromMap(map['info']),
  );
  
  List<Results> results;
  Info info;
  
  Map<String, dynamic> toMap() => {
      'results': results.map((e) => e.toMap()).toList(),
      'info': info.toMap(),
  };
  
}

class Info {
  Info({
      required this.seed,
      required this.results,
      required this.page,
      required this.version,
  });
  
  factory Info.fromMap(Map<String, dynamic> map) => Info(
      seed: map['seed'],
      results: map['results'],
      page: map['page'],
      version: map['version'],
  );
  
  String seed;
  int results;
  int page;
  String version;
  
  Map<String, dynamic> toMap() => {
      'seed': seed,
      'results': results,
      'page': page,
      'version': version,
  };
  
}

class Results {
  Results({
      required this.gender,
      required this.name,
      required this.location,
      required this.email,
      required this.login,
      required this.dob,
      required this.registered,
      required this.phone,
      required this.cell,
      required this.id,
      required this.picture,
      required this.nat,
  });
  
  factory Results.fromMap(Map<String, dynamic> map) => Results(
      gender: map['gender'],
      name: Name.fromMap(map['name']),
      location: Location.fromMap(map['location']),
      email: map['email'],
      login: Login.fromMap(map['login']),
      dob: Dob.fromMap(map['dob']),
      registered: Registered.fromMap(map['registered']),
      phone: map['phone'],
      cell: map['cell'],
      id: Id.fromMap(map['id']),
      picture: Picture.fromMap(map['picture']),
      nat: map['nat'],
  );
  
  String gender;
  Name name;
  Location location;
  String email;
  Login login;
  Dob dob;
  Registered registered;
  String phone;
  String cell;
  Id id;
  Picture picture;
  String nat;
  
  Map<String, dynamic> toMap() => {
      'gender': gender,
      'name': name.toMap(),
      'location': location.toMap(),
      'email': email,
      'login': login.toMap(),
      'dob': dob.toMap(),
      'registered': registered.toMap(),
      'phone': phone,
      'cell': cell,
      'id': id.toMap(),
      'picture': picture.toMap(),
      'nat': nat,
  };
  
}

class Picture {
  Picture({
      required this.large,
      required this.medium,
      required this.thumbnail,
  });
  
  factory Picture.fromMap(Map<String, dynamic> map) => Picture(
      large: map['large'],
      medium: map['medium'],
      thumbnail: map['thumbnail'],
  );
  
  String large;
  String medium;
  String thumbnail;
  
  Map<String, dynamic> toMap() => {
      'large': large,
      'medium': medium,
      'thumbnail': thumbnail,
  };
  
}

class Id {
  Id({required this.name, required this.value});
  
  factory Id.fromMap(Map<String, dynamic> map) => Id(
      name: map['name'],
      value: map['value'],
  );
  
  String name;
  String value;
  
  Map<String, dynamic> toMap() => {
      'name': name,
      'value': value,
  };
  
}

class Registered {
  Registered({required this.date, required this.age});
  
  factory Registered.fromMap(Map<String, dynamic> map) => Registered(
      date: map['date'],
      age: map['age'],
  );
  
  String date;
  int age;
  
  Map<String, dynamic> toMap() => {
      'date': date,
      'age': age,
  };
  
}

class Dob {
  Dob({required this.date, required this.age});
  
  factory Dob.fromMap(Map<String, dynamic> map) => Dob(
      date: map['date'],
      age: map['age'],
  );
  
  String date;
  int age;
  
  Map<String, dynamic> toMap() => {
      'date': date,
      'age': age,
  };
  
}

class Login {
  Login({
      required this.uuid,
      required this.username,
      required this.password,
      required this.salt,
      required this.md5,
      required this.sha1,
      required this.sha256,
  });
  
  factory Login.fromMap(Map<String, dynamic> map) => Login(
      uuid: map['uuid'],
      username: map['username'],
      password: map['password'],
      salt: map['salt'],
      md5: map['md5'],
      sha1: map['sha1'],
      sha256: map['sha256'],
  );
  
  String uuid;
  String username;
  String password;
  String salt;
  String md5;
  String sha1;
  String sha256;
  
  Map<String, dynamic> toMap() => {
      'uuid': uuid,
      'username': username,
      'password': password,
      'salt': salt,
      'md5': md5,
      'sha1': sha1,
      'sha256': sha256,
  };
  
}

class Location {
  Location({
      required this.street,
      required this.city,
      required this.state,
      required this.country,
      required this.postcode,
      required this.coordinates,
      required this.timezone,
  });
  
  factory Location.fromMap(Map<String, dynamic> map) => Location(
      street: Street.fromMap(map['street']),
      city: map['city'],
      state: map['state'],
      country: map['country'],
      postcode: map['postcode'],
      coordinates: Coordinates.fromMap(map['coordinates']),
      timezone: Timezone.fromMap(map['timezone']),
  );
  
  Street street;
  String city;
  String state;
  String country;
  int postcode;
  Coordinates coordinates;
  Timezone timezone;
  
  Map<String, dynamic> toMap() => {
      'street': street.toMap(),
      'city': city,
      'state': state,
      'country': country,
      'postcode': postcode,
      'coordinates': coordinates.toMap(),
      'timezone': timezone.toMap(),
  };
  
}

class Timezone {
  Timezone({required this.offset, required this.description});
  
  factory Timezone.fromMap(Map<String, dynamic> map) => Timezone(
      offset: map['offset'],
      description: map['description'],
  );
  
  String offset;
  String description;
  
  Map<String, dynamic> toMap() => {
      'offset': offset,
      'description': description,
  };
  
}

class Coordinates {
  Coordinates({required this.latitude, required this.longitude});
  
  factory Coordinates.fromMap(Map<String, dynamic> map) => Coordinates(
      latitude: map['latitude'],
      longitude: map['longitude'],
  );
  
  String latitude;
  String longitude;
  
  Map<String, dynamic> toMap() => {
      'latitude': latitude,
      'longitude': longitude,
  };
  
}

class Street {
  Street({required this.number, required this.name});
  
  factory Street.fromMap(Map<String, dynamic> map) => Street(
      number: map['number'],
      name: map['name'],
  );
  
  int number;
  String name;
  
  Map<String, dynamic> toMap() => {
      'number': number,
      'name': name,
  };
  
}

class Name {
  Name({
      required this.title,
      required this.first,
      required this.last,
  });
  
  factory Name.fromMap(Map<String, dynamic> map) => Name(
      title: map['title'],
      first: map['first'],
      last: map['last'],
  );
  
  String title;
  String first;
  String last;
  
  Map<String, dynamic> toMap() => {
      'title': title,
      'first': first,
      'last': last,
  };
  
}

